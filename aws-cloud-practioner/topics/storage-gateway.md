# Storage Gateway

> What is it

* Hybrid cloud storage that gives on prem access to cloud storage

> Use cases

1. Moving backups to the cloud
2. On prem file share backed by cloud storage
3. Low latency access to data for on-prem applications

> There are **3** types of gateways

File gateway

Volume Gateway - block-based access _for_ on-prem 

Tape Gateway

# From Checkpoint 00

> A company has a fleet of cargo ships. The cargo ships have
> sensors that collect data at sea, where there is intermittent
> or no internet connectivity. The company needs to collect,
> format, and process the data at sea and move the data to AWS
> later.  Which AWS service should the company use to meet
> these requirements?

In this case the key points in the question are the intermittent
internet conectivity and the fact that they need to back up
data to the cloud in general.


