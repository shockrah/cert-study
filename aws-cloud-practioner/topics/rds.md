# RDS

From this question:

```
Questions 4: Which design principles are enabled by the AWS Cloud to improve the operation of workloads? (Select TWO)

A. Minimize platform design
B. Loose coupling
C. Customized hardware
D. Remove single points of failure
E. Minimum viable product
The correct answer is B. “Loose coupling” and D. “Remove single points of failure”.

Explanation:

Loose coupling is when you break systems down into smaller components that are loosely coupled together. This reduces interdependencies between systems components. This is achieved in the cloud using messages buses, notification and messaging services.

Removing single points of failure ensures fault tolerance and high availability. This is easily achieved in the cloud as the architecture and features of the cloud support the implementation of highly available and fault tolerant systems.

A. “Minimize platform design” is incorrect. This is not an operational advantage for workloads in the cloud.

C. “Customized hardware” is incorrect. You cannot customize hardware in the cloud.

E. “Minimum viable product” is incorrect. This is not an operational advantage for workloads in the cloud.

References:

https://d1.awsstatic.com/whitepapers/AWS_Cloud_Best_Practices.pdf

https://digitalcloud.training/certification-training/aws-certified-cloud-practitioner/architecting-for-the-cloud/

```

# Best Practices with Databases

## Removing Single Points of Failure

* You can distrubute compute instances across subnets and availability zones

* Regional Failover is also much easier to configure than with traditional infrastructure

Traditionally you would need multiple sites but RDS offers this by default

##  Loose Coupling

Because AWS has lots of "little services" that we can use to build out our architecture
we can more easily create an infrastructure that is more well suited to being loosely coupled. In the case of RDS if we have 2 seperate Mysql clusters and want to update one
of their versions then we could do this without disturbing the other because RDS allows
us to seperate these types of services more easily.

Basically everything in AWS is seperate until connected in some way.



## General Advantages of RDS

In traditional infrastructure there are limiting factors such as 
physical equipement and licensing cost to support their use cases.
With AWS these constraints are removed because AWS takes care of both
the hard ware and licensing cost.

One example for the licensing cost issue is the case for Windows servers:

> Where normally you must pay an explicit license for Windows server usage
> the license is dealt with by the (typically) increased hosting cost




