# EBS



# EC2


# From Checkpoint 00

> A retail company needs to build a highly available architecture
> for a new ecommerce platform. The company is using only AWS
> services that replicate data across multiple Availability
> Zones.
> Which AWS services should the company use to meet this 
> requirement? (Choose two.)


A. Amazon EC2
B. Amazon Elastic Block Store (Amazon EBS)
C. Amazon Aurora
D. Amazon DynamoDB
E. Amazon Redshift

Answer `AB`.

Key points here firstly is that EBS is automatically replicated 
within its availaibility zone. This by itself provides high
availability for our storage however there is still the question
the computing side. EC2 allows us to create instances and data in
multiple places to support the high availability.

# References

* https://digitalcloud.training/aws-storage-services/#amazon-elastic-block-store-ebs

* https://digitalcloud.training/aws-compute-services/#amazon-ec2
