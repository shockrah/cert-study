# NACL

Virtual firewall at the subnet level

* Create allow **and deny** rules

Permits to block specific IP's

# Security Groups

Firewalls at the instance level

* Starts out blocking _everything_ but slowly opens up with more rules

* Unable to create deny rules

* Only _allow_ rules can be added

# Generally 

NACL's are useful for when we want to apply some kind of filter to
a whole subnet and its instance members. Security groups are better
for instance/service specific rules.

