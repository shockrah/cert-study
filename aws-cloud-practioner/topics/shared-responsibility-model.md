# Share Responsibility - What Customers (us) and AWS manage

## Context

From this question: 

```
Which IT controls do AWS and the customer share, according to the AWS shared responsibility model? (Choose two.)

A. Physical and environmental controls
-> B. Patch management
-> C. Cloud awareness and training
D. Zone security
E. Application data encryption
```

Tried `CE`, real answer was `BC`.

## Notes

> Where our side of responsibility begins

We handle the security **in** the cloud. Basically this means we have to deal with the guest operating system and anything that 
we run "on top" of AWS's services. For ec2 this would include the guest OS and upwards. For things like EKS, Fargate this would 
mean taking care of the docker images that are being used. Any data that we populate and modify is also our problem.



> Where Amazon's responsibility begins

They handle the security **of** the cloud. Ensuring the hard ware is up to spec and functioning properly to allow customers
to do their work without issue is one of their major concerns.

> What both must handle, in what is usually referred to as shared controls

1. Patch management: AWS must fix flaws within the infrastructure but customers must patch the software they run on top of this
infrastructure

2. Configuration Management: AWS handles configuring the infrastructure devices which allows precedence for customers to
configure their own guest OS's, DB's and other things.

3. Both must train their employees so that everyone knows what is going on.

## Short Gist

Basically anything that we can provision is the responsibility of Amazon to ensure it is working as advertised. Security groups
should work expected and not block traffic we don't specify, VPC's should provide isolation correctly, RDS should be exposed to
only the net's that request, and etc. What _is_ our problem is ensuring we setup our SG's correctly, ensure databases are well
configured to not expose data improperly, and so on.





