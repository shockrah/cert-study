# Context

From this question:

```
A company wants to migrate its applications from its on-premises data center to a VPC in the AWS Cloud. These applications will need to access on-premises resources.
Which actions will meet these requirements? (Choose two.)

A. Use AWS Service Catalog to identify a list of on-premises resources that can be migrated.
B. Create a VPN connection between an on-premises device and a virtual private gateway in the VPC.
C. Use an Amazon CloudFront distribution and configure it to accelerate content delivery close to the on-premises resources.
D. Set up an AWS Direct Connect connection between the on-premises data center and AWS.
E. Use Amazon CloudFront to restrict access to static web content provided through the on-premises web servers.
```

My Response: BD

Real Answer: AD


## AWS Service Catalog

Service that allows us to centrally control "products" that are comprised of various
AWS resources which make up a real product. For example a product could be a simple
Linux box or a whole EKS cluster with a DB and failover. Both of which could be managed
through the service catalog.
With this in mind we can then deploy whole products much faster because it's part of
our approved catalog.

## Direct Connect

This service provides a way to connect on-prem resources to resources in AWS that 
doesn't involve using the internet. This means private VPC's can talk to private
on-prem resources through the Direct Connect link rather than go through clearnet.

Things that you can do with Direct Connect involved:

* Reach all AZ's in the region from the DX connection

* Establish IPSec connections over public VIF to remote regions

* High availability (HA) is possible with 2 DX connections

    * Valid configurations include: active/active & standby/active

One thing to note:

* Route tables must be updated to accomodate the new DX connection





