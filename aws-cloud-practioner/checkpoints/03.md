# From exampro's free practice test 

57/65
88%

# Corrections

## Question 10
> What service helps you to aggregate logs from your EC2 instance?

*Wrong*: Cloudtrail 

*Right*: Cloudwatch logs

Keywords here is `logs from`, as cloudtrail only watches for API events which aren't necessarily being made by an EC2 instance.

## Question 22

> Which one of the following features is normally present in all AWS Support plans?

*Wrong*: A dedicated support person

*Right*: 24/7 acess to customer service

## Question 29

> There is a requirement to move a 10 TB data warehouse to the AWS cloud. Which of the following is an ideal service which can be used to move this amount of data to the AWS Cloud?

*Wrong*: Amazon S3 MulitPart Upload

*Right*: AWS Snowcone SSD

Snowcone is a **device** built for edge computing and data transfer. For this reason it can be used to move data into
AWS cloud either by shipping the device to AWS or online by using AWS DataSync.
There are also 2 flavors of snowcone:

* Snowcone - 2vCPU's, 4 GB memory, 8 TB HDD storage
* Snowcone SSD - 2 vCPU's, 4 GB memory, 14 TB ssd storage

## Question 35

> You have a distributed application that regularly processes large amounts of data across multiple Amazon EC2 instances and can recover smoothly from interruptions. This application does not require continuous, dedicated compute capacity. What is the most cost-effective way to ensure the availability of this application?

*Wrong*: on-demand instances

*Right*: Spot instances

For cost-effectiveness spot or reserved instances are the best options. However reserved is not a good answer for this case
because we're looking to scale down at some point in terms of cost and rerserved instances won't reduce their cost on the scale down.

## Question 36

```
What are the characteristics of Amazon S3? (Select 2)

S3 should be used to host a relational database.
Objects are directly accessible via a URL.  SELECTED GOOD
S3 allows you to store unlimited amounts of data. NOT_SELECTED CORRECT
S3 allows you to store objects of virtually unlimited size. SELECTED WRONG
```

Keep in mind that while we get virtually unlimited storage capacity we don't get an ulimited object size.
The limit per object is 5 TB.

## Question 55

> Which of the following AWS services should you use to migrate an existing database to AWS?

*Wrong*: AWS Storage Gateway

*Right*: AWS DMS

This stands for Database Migration Services which allows the source database to remain fully operational
during the migration.


## Question 59

> Which of the following options of AWS RDS allows for AWS to failover to a secondary database in case the primary one fails?

*Wrong*: AWS Failover

*Right*: AWS Multi-AZ

Multi-az allows RDS to setup instances in other AZ's allowing for more fault-tolerance and thus in this case
failover abilities.


## Question 65

> A company is deploying a two-tier, highly available web application to AWS. Which service provides durable storage for static content while utilizing lower Overall CPU resources for the web tier?

*Wrong*: EBS Volume

*Right*: S3

The key here is that S3 is more than suitable for static content hosting and is also highly available.





