# Study Guide Material

* https://digitalcloud.training/category/aws-cheat-sheets/aws-cloud-practitioner/

# Practice Exams

* https://www.itexams.com/exam/AWS-Certified-Cloud-Practitioner

* https://www.testpreptraining.com/aws-cloud-practitioner-exam-questions

# Flashcards

https://quizlet.com/504388295/aws-certified-cloud-practitioner-clf-c01-exam-questions-flash-cards/
